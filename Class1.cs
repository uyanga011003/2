﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class class1
    {
        static void Main(string[] args)
        {

            // Создаем объекты фотографий
            IPhoto photo1 = new Photo("photo1.jpg", 800, 600);
            photo1.Display();

            // Создаем декораторы
            IPhoto addframe = new AddFrame(photo1, "Виньетка");
            addframe.Display();
            IPhoto improveQuality = new ImproveQuality(photo1, 50);
            improveQuality.Display();

            // Создаем альбом и добавляем фотографии
            PhotoAlbum album = new PhotoAlbum();
            PhotoAlbumView albumView = new PhotoAlbumView("альбом1", album);
            album.AddPhoto(photo1);
            album.RemovePhoto(photo1);

            Console.ReadKey();
        }
    }


    public interface IPhoto
    {
        IPhoto Clone();
        void Display();
    }
    public class Photo : IPhoto
    {
        string file_path;
        int width;
        int height;
        public Photo(string fp, int w, int h)
        {
            file_path = fp;
            width = w;
            height = h;
        }
        public IPhoto Clone()
        {
            return new Photo(this.file_path, this.width, this.height);
        }
        public void Display()
        {
            Console.WriteLine($"Фотография длиной {height} и шириной {width}");
        }
    }


    public abstract class PhotoEditor : IPhoto
    {
        protected IPhoto photo;
        public PhotoEditor(IPhoto ph)
        {
            this.photo = ph;
        }
        public virtual void Display()
        {
            photo.Display();
        }

        public virtual IPhoto Clone()
        {
            return photo.Clone();
        }
    }


    public class AddFrame : PhotoEditor
    {
        string frame;
        public AddFrame(IPhoto ph, string fr) : base(ph)
        {
            this.frame = fr;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine("К фотографии добавлена рамка");
        }
    }
    public class ImproveQuality : PhotoEditor
    {
        private int level;
        public ImproveQuality(IPhoto ph, int l) : base(ph)
        {
            this.level = l;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine("Улучшено качество фотографии");
        }
    }



    interface IObserver
    {
        void Update(List<IPhoto> photos);
    }
    interface IObservable
    {
        void RegisterObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }
    class PhotoAlbum : IObservable
    {
        List<IPhoto> photos = new List<IPhoto>();
        List<IObserver> observers = new List<IObserver>();
        public PhotoAlbum()
        {
            observers = new List<IObserver>();
        }
        public void RegisterObserver(IObserver o)
        {
            observers.Add(o);
        }
        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }
        public void NotifyObservers()
        {
            foreach (IObserver o in observers)
            {
                o.Update(photos);
            }
        }
        public void AddPhoto(IPhoto photo)
        {
            photos.Add(photo);
            Console.WriteLine("Фотография добавлена в альбом");
            NotifyObservers();
        }
        public void RemovePhoto(IPhoto photo)
        {
            photos.Remove(photo);
            Console.WriteLine("Фотография удалена");
            NotifyObservers();
        }
    }
    class PhotoAlbumView : IObserver
    {
        public string Name { get; set; }
        IObservable album;
        public PhotoAlbumView(string name, IObservable obs)
        {
            this.Name = name;
            album = obs;
            album.RegisterObserver(this);
        }
        public void Update(List<IPhoto> photos)
        {
            Console.WriteLine("Интерфейс обновлен с новым списком фотографий");
        }
    }


}
