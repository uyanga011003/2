 public abstract class PhotoEditor : IPhoto
    {
        protected IPhoto photo;
        public PhotoEditor(IPhoto ph)
        {
            this.photo = ph;
        }
        public virtual void Display()
        {
            photo.Display();
        }

        public virtual IPhoto Clone()
        {
            return photo.Clone();
        }
    }


    public class AddFrame : PhotoEditor
    {
        string frame;
        public AddFrame(IPhoto ph, string fr):base(ph)
        {
            this.frame = fr;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine("К фотографии добавлена рамка");
        }
    }
    public class ImproveQuality : PhotoEditor
    {
        private int level;
        public ImproveQuality(IPhoto ph, int l):base(ph)
        {
            this.level = l;
        }
        public override void Display()
        {
            base.Display();
            Console.WriteLine("Улучшено качество фотографии");
        }
    }
