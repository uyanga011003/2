interface IObserver
    {
        void Update(List<IPhoto> photos);
    }
    interface IObservable
    {
        void RegisterObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }
    class PhotoAlbum : IObservable
    {
        List<IPhoto> photos = new List<IPhoto> ();
        List<IObserver> observers = new List<IObserver>();
        public PhotoAlbum()
        {
            observers = new List<IObserver>();
        }
        public void RegisterObserver(IObserver o)
        {
            observers.Add(o);
        }
        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }
        public void NotifyObservers()
        {
            foreach (IObserver o in observers)
            {
                o.Update(photos);
            }
        }
        public void AddPhoto(IPhoto photo)
        {
            photos.Add(photo);
            Console.WriteLine("Фотография добавлена в альбом");
            NotifyObservers();
        }
        public void RemovePhoto(IPhoto photo)
        {
            photos.Remove(photo);
            Console.WriteLine("Фотография удалена");
            NotifyObservers();
        }
    }
    class PhotoAlbumView : IObserver
    {
        public string Name { get; set; }
        IObservable album;
        public PhotoAlbumView(string name, IObservable obs)
        {
            this.Name = name;
            album = obs;
            album.RegisterObserver(this);
        }
        public void Update(List<IPhoto> photos)
        {
            Console.WriteLine("Интерфейс обновлен с новым списком фотографий");
        }
    }
