
    interface IPhoto
    {
        IPhoto Clone();
        void GetInfo();
    }
    class Photo : IPhoto
    {
        string file_path;
        int width;
        int height;
        public Photo(string fp, int w, int h)
        {
            file_path = fp;
            width = w;
            height = h;
        }
        public IPhoto Clone()
        {
            return new Photo(this.file_path, this.width, this.height);
        }
        public void GetInfo()
        {
            Console.WriteLine($"Фотография длиной {height} и шириной {width}");
        }
    }
